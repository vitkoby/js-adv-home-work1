import Employee from './script.js';

class Programmer extends Employee {
	constructor(options, lang) {
		super(options);
		this.lang = lang;
	}

	get someIncome () {
		return this.salary * 3;
	} 
}

const vova = new Programmer ({
	name: 'Vova',
	age: 25,
	salary: 1200
}) 

const marina = new Programmer ({
	name: 'Marina',
	age: 22,
	salary: 800
}) 

console.log(vova, marina);
console.log(vova.someIncome, marina.someIncome);





