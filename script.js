// Ответы на теоритеческие вопросы: 
// 1. Если нужно прочитать свойсво, а его нету, тогда JS берет его из прототипа.
// 2. super() вызывает родительский консруктор с его характеристиками

export default class Employee  {
	constructor (options) {
		this.name = options.name;
		this.age = options.age;
		this.salary = options.salary;
	}

	get someIncome () {
		return this.salary;
	} 
}




